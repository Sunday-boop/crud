package com.example.crud;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class CreatePetActivity extends AppCompatActivity {
    Button btn_add_pet;
    Button btn_cu_photo, btn_r_photo;
    EditText nombre, edad, color;
    ImageView photo_pet;
    private FirebaseFirestore mfirestore;

    StorageReference storageReference;
    String storage_path = "pet/*";

    private static final int COD_SEL_STORAGE = 200;
    private static final int COD_SEL_IMAGE = 300;
    private Uri image_url;
    String photo = "photo";
    String idd;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pet);

        this.setTitle("Crear mascota");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String id = getIntent().getStringExtra("id_pet");
        mfirestore = FirebaseFirestore.getInstance();

        nombre = findViewById(R.id.nombre);
        edad = findViewById(R.id.edad);
        color = findViewById(R.id.color);
        btn_add_pet = findViewById(R.id.btn_add_pet);
        storageReference = FirebaseStorage.getInstance().getReference();
        photo_pet = findViewById(R.id.pet_photo);
        btn_cu_photo = findViewById(R.id.btn_photo);

        btn_cu_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadPhoto();
            }
        });

        if (id == null || id == ""){
            btn_cu_photo.setVisibility(View.GONE);
            photo_pet.setVisibility(View.GONE);
            btn_add_pet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String nombre_pet = nombre.getText().toString();
                    String edad_pet = edad.getText().toString();
                    String color_pet = color.getText().toString();

                    if (nombre_pet.isEmpty() && edad_pet.isEmpty() && color_pet.isEmpty()){
                        Toast.makeText(getApplicationContext(), "Ingresar los datos", Toast.LENGTH_SHORT).show();
                    }else {
                        postPet(nombre_pet,edad_pet,color_pet);
                    }
                }
            });
        }else {
            idd = id;
            btn_add_pet.setText("Actualizar");
            getPet(id);
            btn_add_pet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String nombre_pet = nombre.getText().toString();
                    String edad_pet = edad.getText().toString();
                    String color_pet = color.getText().toString();

                    if (nombre_pet.isEmpty() && edad_pet.isEmpty() && color_pet.isEmpty()){
                        Toast.makeText(getApplicationContext(), "Ingresar los datos", Toast.LENGTH_SHORT).show();
                    }else {
                        updatePet(nombre_pet,edad_pet,color_pet, id);
                    }
                }
            });
        }
    }

    private void uploadPhoto() {
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");

        startActivityForResult(i, COD_SEL_IMAGE);
    }

    private void updatePet(String nombre_pet, String edad_pet, String color_pet, String id) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nombre_pet);
        map.put("edad", edad_pet);
        map.put("color", color_pet);

        mfirestore.collection("pet").document(id).update(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(getApplicationContext(), "Actualizado correctamente", Toast.LENGTH_SHORT).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error al actualizar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void postPet(String nombre_pet, String edad_pet, String color_pet) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nombre_pet);
        map.put("edad", edad_pet);
        map.put("color", color_pet);

        mfirestore.collection("pet").add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(getApplicationContext(), "Mascota agregada correctamente", Toast.LENGTH_SHORT).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error al agregar mascota", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPet(String id){
        mfirestore.collection("pet").document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String namePet = documentSnapshot.getString("nombre");
                String agePet = documentSnapshot.getString("edad");
                String colorPet = documentSnapshot.getString("color");
                String photoPet = documentSnapshot.getString("foto");
                nombre.setText(namePet);
                edad.setText(agePet);
                color.setText(colorPet);
                try {
                    if (!photoPet.equals("")){
                        Toast toast = Toast.makeText(getApplicationContext(), "Cargando foto", Toast.LENGTH_SHORT);
                        toast.show();
                        toast.setGravity(Gravity.TOP, 0, 200);
                        toast.show();
                        Picasso.with(CreatePetActivity.this).load(photoPet).resize(300, 300).into(photo_pet);
                    }
                }catch (Exception e){
                    Log.v("Error","e:" + e);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Error al obtener los datos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == COD_SEL_IMAGE){
                image_url = data.getData();
                subirPhoto(image_url);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void subirPhoto(Uri image_url) {

        String rute_storage_photo = storage_path + "" + photo + "" + idd;
        StorageReference reference = storageReference.child(rute_storage_photo);
        reference.putFile(image_url).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                while (!uriTask.isSuccessful());
                if (uriTask.isSuccessful()){
                    uriTask.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String download_uri = uri.toString();
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("foto", download_uri);
                            mfirestore.collection("pet").document(idd).update(map);
                            Toast.makeText(CreatePetActivity.this, "Foto Actualizada", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CreatePetActivity.this, "Error al cargar imagen", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return  false;
    }
}