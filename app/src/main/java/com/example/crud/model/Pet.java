package com.example.crud.model;

public class Pet {
    String nombre,color,edad, foto;
    public Pet(){}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEdad() { return edad; }

    public void setEdad(String edad) { this.edad = edad; }

    public String getFoto() { return foto; }

    public void setFoto(String foto) { this.foto = foto; }

    public Pet(String nombre, String color, String edad, String foto) {
        this.nombre = nombre;
        this.color = color;
        this.edad = edad;
        this.foto = foto;
    }
}
